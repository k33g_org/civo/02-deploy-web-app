# 02-deploy-web-app

## Requirements

Read: https://gitlab.com/k33g_org/civo/01-creating-a-cluster/-/blob/main/README.md?ref_type=heads
> You need a Kubernetes cluster

## Objectives

- Dockerize the "hello-world" Go application (`./hello-world`)
- Deploy the "hello-world" Go application on Kubernetes

## Dockerize the Go application

Create a `Dockefile` into `./hello-world` with the following content:

```Dockerfile
# Build the application from source
FROM golang:1.20 AS build-stage

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY *.go ./

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o hello-world

# Deploy the application binary into a lean image
FROM scratch

WORKDIR /

COPY --from=build-stage /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build-stage /app/hello-world .
EXPOSE 8080
CMD ["/hello-world"]
```
> We uses the Multi-stage builds method, then the Go application is built (compiled) when the building of the Docker image.

### Build the image

To build the Docker image of the application, use the following commands:

```bash
IMAGE_NAME="demo-hello-world"
docker build -t ${IMAGE_NAME} . 
docker images | grep ${IMAGE_NAME}
```
> The last command will display the size og the image. As the last layer of the build is a `scratch` image, the size of the image is pretty small (`<7MB`). It could be useful for demonstrations.

### Test the image

To test the Docker image of the application, use the following commands:

```bash
IMAGE_NAME="demo-hello-world"
HTTP_PORT=8080
docker run \
-p ${HTTP_PORT}:${HTTP_PORT} \
${IMAGE_NAME}
```

Then you can call the service like this: `curl http://localhost:8080`

### Publish the image on the Docker Hub

To publish the Docker image of the application on the Docker Hub, use the following commands:

```bash
IMAGE_NAME="demo-hello-world"
IMAGE_TAG="0.0.0"
docker login -u ${DOCKER_USER} -p ${DOCKER_PWD}
docker tag ${IMAGE_NAME} ${DOCKER_USER}/${IMAGE_NAME}:${IMAGE_TAG}
docker push ${DOCKER_USER}/${IMAGE_NAME}:${IMAGE_TAG}
```
> TODO: make a §📝 to explain how to publish on the GitLab registry

### Test the image remotely

To test the Docker image from the Docker Hub, use the following commands:

```bash
IMAGE_NAME="demo-hello-world"
IMAGE_TAG="0.0.0"
HTTP_PORT=8080

docker run \
-p ${HTTP_PORT}:${HTTP_PORT} \
${DOCKER_USER}/${IMAGE_NAME}:${IMAGE_TAG}
```

## Deploy the application on a Kubernetes cluster

We need a yaml manifest to describe the Service, the Deployment and the Ingress. Create into the `hello-world` directory a `deploy.yaml` file with the following content:

```yaml
---
# Service
apiVersion: v1
kind: Service
metadata:
  name: demo-hello-world
spec:
  selector:
    app: demo-hello-world
  ports:
    - port: 80
      targetPort: 8080
---
# Deployment
apiVersion: apps/v1
kind: Deployment
metadata:
  name: demo-hello-world
spec:
  replicas: 1
  selector:
    matchLabels:
      app: demo-hello-world
  template:
    metadata:
      labels:
        app: demo-hello-world
    spec:
      containers:
        - name: demo-hello-world
          image: k33g/demo-hello-world:0.0.0
          ports:
            - containerPort: 8080
          imagePullPolicy: Always

---
# Ingress
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: demo-hello-world
spec:
  rules:
    - host: demo-hello-world.49ef064a-6dc0-4c4a-b9c6-50b8cad90450.k8s.civo.com
      http:
        paths:
        - path: /
          pathType: Prefix
          backend:
            service: 
              name: demo-hello-world
              port: 
                number: 80
```

The value of the `host` key of the **Ingress** section, is composed by the name of the application (`demo-hello-world`) and the DNS name of the Kubernetes cluster.

To get the DNS name of a Civo K8S cluster, you can use this command:
```bash
CLUSTER_REGION="FRA1"
CLUSTER_NAME="first-cluster"

civo --region=${CLUSTER_REGION} kubernetes show ${CLUSTER_NAME}
```
> ✋ adapt if you don't use Civo

### Deploy

To deploy the application, use the following commands:
```bash
cd hello-world
export KUBECONFIG=$PWD/../config/k3s.yaml
kubectl apply -f ./deploy.yaml
```

You can check with K9S if the application is deployed.

#### Get the url of the deployed web application

```bash
export KUBECONFIG=$PWD/config/k3s.yaml

WEBAPP_NAME="demo-hello-world"
KUBE_NAMESPACE="default"
kubectl describe ingress ${WEBAPP_NAME} -n ${KUBE_NAMESPACE}
```

Then you can curl the web application, ex `curl http://demo-hello-world.49ef064a-6dc0-4c4a-b9c6-50b8cad90450.k8s.civo.com`

### Remove the application

To remove (undeploy) the application, use the following commands:
```bash
cd hello-world
export KUBECONFIG=$PWD/../config/k3s.yaml
kubectl delete -f ./deploy.yaml
```

🎉 That's all


